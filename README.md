Jxls Jexcel
============

Jxls Overview
--------
[Jxls](http://jxls.sf.net/) is a small and simple to use Java library for Excel generation.

Jxls abstracts Excel generation from underlying java-to-excel low-level processing library.

Jxls uses a special markup in Excel templates to define output formatting and data layout.

Module overview
---------------
The module contains source code for [Jxls Jexcel Transformer](http://jxls.sf.net/reference/main_concepts.html)
and is based on [Java Excel API](http://jexcelapi.sourceforge.net/)

Important news for jxls-jexcel users
--------------
jxls 2.6.0 is the last jxls version coming with `jxls-jexcel` support.
Starting from jxls 2.7.0  we will not be providing any updates for `jxls-jexcel` module. Please use `jxls-poi`.
